import json

from serialization_csv import read_sample_csv


data = read_sample_csv()

with open('Sample100.json', 'w') as f:
    json.dump(data, f, indent=4)
