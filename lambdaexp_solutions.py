"""given the function

```
def sum_tuple(t: Tuple[int, int]) -> int:
  a, b = t
  return a + b
```

rewrite it using a lambda expression."""

ex1 = lambda t: t[0] + t[1]

"""given the function

```
def squares_and_cubes(ns: List[int]) -> List[Tuple[int, int]]:
  sqs_cbs = []
  for n in ns:
    sqs_cbs.append((n * n, n ** 3))
  return sqs_cbs
```

rewrite it using a lambda expression."""

ex2 = lambda ns: [(n * n, n ** 3) for n in ns]


"""write a lambda expression that, given an input `List[List[int]]`, returns a `List[int]`, where the elements of the output list are the first element of each sublist of the input list.

example: for a given input list `[[1, 2, 3], [4, 5, 6], [7, 8, 9]]`,
the output should be `[1, 4, 7]`

If writing the lambda expression directly is difficult, try defining it as a named function (`def`) first."""

ex3 = lambda ns: [l[0] for l in ns]


#given the list
people = [{"name": "Alfred", "occupation": "butler", "age": 62},
          {"name": "Bob", "occupation": "gardener", "age": 35},
          {"name": "Caroline", "occupation": "cook", "age": 29},
          {"name": "David", "occupation": "security", "age": 31}]


"""a. write a lambda expression L such that `sorted(people, key=L)` returns

```
[{"name": "Caroline", "occupation": "cook", "age": 29},
 {"name": "David", "occupation": "security", "age": 31},
 {"name": "Bob", "occupation": "gardener", "age": 35},
 {"name": "Alfred", "occupation": "butler", "age": 62}]
```"""
print(sorted(people, key=lambda person: person['age']))

"""b. write a lambda expression L such that `sorted(people, key=L)` returns

```
[{"name": "David", "occupation": "security", "age": 31},
 {"name": "Bob", "occupation": "gardener", "age": 35},
 {"name": "Caroline", "occupation": "cook", "age": 29},
 {"name": "Alfred", "occupation": "butler", "age": 62}]
```"""
print(sorted(people, key=lambda person: person['occupation'], reverse=True))

"""c. write a lambda expression L such that `min(people, key=L)` returns
`{"name": "Alfred", "occupation": "butler", "age": 62}`"""
print(min(people, key=lambda p: -p['age']))
print(min(people, key=lambda p: p['name']))
print(min(people, key=lambda p: p['occupation']))

"""d. write a lambda expression L such that `max(people, key=L)` returns
`{"name": "David", "occupation": "security", "age": 31}`"""
print(max(people, key=lambda p: p['name']))
print(max(people, key=lambda p: p['occupation']))

# Figuring out what is being sorted, and how, is part of the exercise.