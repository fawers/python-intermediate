def run_1():
    a = 3
    b = [1, 0, 2]
    for elem in b:
        reslt = a / elem
        print(f"Result is: {reslt}")


def run_2():
    a = 3
    b = [1, 0, 2]
    for elem in b:
        try:
            reslt = a / elem

        except ZeroDivisionError:
            print(f"Can't divide {a} by {elem}")
            continue

        print(f"Result is: {reslt}")


def run_3(planet: dict):
    key = "rotation_period"

    try:
        print(f"Rotation period of the planet is {planet[key]}")

    except KeyError as ke:
        print(f"Invalid planet data: {planet}")
        print(f"Info on error: {repr(ke)}")


def run_4(students: list):
    try:
        print(f"The last student in the list is {students[14]}")

    except IndexError as ie:
        print("No student at index 14")
        print(f"Info on error: {ie}")


#run_3({'rotation_period': 24, 'orbital_period': 365})
#run_3({'rotion_period': 27, 'orbital_period': 300})

students = list('abcdefghijklmno')
#run_4(students)
#run_4(students[:-1])

list_of_ints = [1,2,3,4,5]
#              ^ ^ ^ ^ ^

# copy of list
list_of_ints[:]
# every element except the first one
list_of_ints[1:]
# the 2 first elements
list_of_ints[:2]
# rule of thumb, 3 elements starting from n
n = 1
#print(list_of_ints[n:n+3])
# every even index
#print(list_of_ints[::2])
# every odd index
#print(list_of_ints[1::2])
# reversing the list
#print(list_of_ints[::-1])
# reversing and every snd element
#print(list_of_ints[::-2])

# arguments to slicing are in the exact same order as the range function

another_list = [0,1,2,3,4,5,6,7,8,9]
#print(another_list[2:8:2])
#print(list(range(2, 8, 2)))


def run_5(list_of_dicts):
    try:
        value = list_of_dicts[1]['key']
        print(value.first_character)

    except IndexError:
        print("List does not have elements at index 1")

    except KeyError:
        print("Dictionary doesn't have key 'key'")

    except Exception as e:
        print(f"Something bad happened: {(repr(e))} :(")


# run_5([{}, {'key': 'value'}])
# run_5([])
# run_5([{}, {}])

def run_6():
    for d in (0, 1, 0):
        try:
            print(5 / d)

        except ZeroDivisionError:
            print(ZeroDivisionError)
            return -1

        finally:
            print("finally here")
            return 1  # <-- careful


#print(run_6())


def run_7():
    a = 3
    b = [1, 0, 2]
    for elem in b:
        if elem == 0:
            raise ValueError("The divisor cannot be zero")
        result = a / elem
        print(f"Result is: {result}")

try:
    pass
    #run_7()

except Exception as e:
    print(f'boom: {repr(e)}')


class CustomException1(Exception):
    pass


def run_8():
    a = 3
    b = [1, 0, 2]
    for elem in b:
        if elem == 0:
            raise CustomException1("The divisor cannot be zero")
        result = a / elem
        print(f"Result is: {result}")


#run_8()


class CustomException2(Exception):
    def __init__(self):
        message = "THE MESSAGE: The divisor cannot be zero"
        super().__init__(message)


def run_9():
    a = 3
    b = [1, 0, 2]
    for elem in b:
        if elem == 0:
            raise CustomException2()
        result = a / elem
        print(f"Result is: {result}")


#run_9()


class Parent:
    def __init__(self):
        print("In Parent class")


class Child(Parent):
    def __init__(self):
        print("In child class")
        #super().__init__()  # <-- uncomment


Child()