from typing import List, Optional
from abc import ABC, abstractmethod


class Card:
    def __init__(self, *args, **kwargs):
        pass


class Player(ABC):
    def __init__(self, id):
        self.__id = id
        self.__hand: List[Card] = []

    def __repr__(self):
        return f"Player({self.__id})"

    def draw(self, deck: List[Card]):
        self.__hand.append(deck.pop(0))

    @abstractmethod
    def play(self, card_on_stack: Card) -> Optional[Card]:
        pass

    def _get_playable_cards(self, card_on_stack: Card) -> List[Card]:
        return [c for c in self.__hand if c.can_be_placed_onto(card_on_stack)]


class RandomPlayModeCPUPlayer(Player):
    def play(self, card_on_stack: Card) -> Optional[Card]:
        print("In RandomPlayModeCPUPlayer")


class BestPlayModeCPUPlayer(Player):
    def play(self, card_on_stack: Card) -> Optional[Card]:
        print("In BestPlayModeCPUPlayer")


class HumanPlayer(Player):
    def play(self, card_on_stack: Card) -> Optional[Card]:
        print("In HumanPlayer")


game_players: List[Player] = [
    HumanPlayer(1), RandomPlayModeCPUPlayer(2),
    BestPlayModeCPUPlayer(3), RandomPlayModeCPUPlayer(4)]


for player in game_players:
    player.play(Card())  # Knows exactly which play method it has to call
