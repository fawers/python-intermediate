import json
from datetime import datetime


def try_dict_to_json(f):
    def wrapper(*args, **kwargs):
        value = f(*args, **kwargs)

        if isinstance(value, dict):
            try:
                value = json.dumps(value)
                return value

            except TypeError:
                return value

        else:
            return value

    return wrapper


@try_dict_to_json
def valid_json_dict():
    return {"good and cool": "nice"}


@try_dict_to_json
def invalid_json_dict():
    return {"dates don't work that well": datetime.now()}


@try_dict_to_json
def type_is_not_dict():
    return 42  # must remain int


@try_dict_to_json
def this_returns_float():
    return 3.14  # must remain float


assert valid_json_dict() == '{"good and cool": "nice"}'
assert isinstance(invalid_json_dict(), dict)
assert isinstance(type_is_not_dict(), int)
assert isinstance(this_returns_float(), float)
