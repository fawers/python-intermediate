from contextlib import contextmanager


def run_1():
    with open("profiling.py") as f:
        print("Inside WITH scope")
        print(f.closed)

    print("Outside WITH scope")
    print(f.closed)

    print("Using open() and .close()")
    f = open('profiling.py')
    print(f.closed)
    f.close()
    print(f.closed)


class FileReader:
    def __init__(self, filename):
        print("in __init__")
        self.filename = filename
        self.mode = 'r'

    # runs when we use the object in a with block
    # with FileReader(...) ...
    def __enter__(self):
        print("in __enter__")
        self.file = open(self.filename, self.mode)
        return self.file
        #      ^^^^^^^^^ variable in "as f"

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("in __exit__")
        self.file.close()
        print(exc_val)
        # return True if you want to suppress the error


def run_2():
    fr = FileReader('profiling.py')
    print("BEFORE with")
    with fr as f:
        print("AFTER __enter__")
        print(f.read())
        print("BEFORE __exit__")
        raise Exception("Let's see what happens here")
    print("AFTER with")


@contextmanager
def file_reader(filename):
    print("in file_reader()")
    f = open(filename, mode='r')
    print("before yielding")
    yield f
    print("after yielding")
    f.close()
    print("after .close()")


def run_3():
    with file_reader('profiling.py') as f:
        print(f.read())


run_3()
