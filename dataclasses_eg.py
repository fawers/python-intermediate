from copy import deepcopy
from dataclasses import dataclass

from abc_shape import Shape


@dataclass
class Rectangle(Shape):
    a: int
    b: int
    c: list

    def perimeter(self):
        return 2 * (self.a + self.b)

    def area(self):
        return self.a * self.b


def run_1():
    r1 = Rectangle(4, 5)
    r2 = Rectangle(5, 4)

    print('r1', r1.perimeter(), r1.area())
    print('r2', r2.perimeter(), r2.area())

    print(r1 == r2)
    print(r1 == Rectangle(4, 5))

    print(r1, r2)


def run_2():
    r1 = Rectangle(5, 5)
    l = [r1]
    shallow_copy = l[:]  # same as list(l)
    deep_copy = deepcopy(l)

    print("Before changing: ", l, shallow_copy, deep_copy)
    r1.a, r1.b = 3, 7
    print("After changing:  ", l, shallow_copy, deep_copy)
    print()
    print(r1 is shallow_copy[0])
    print(r1 is deep_copy[0])


def run_3():
    r1 = Rectangle(3, 4, [1,2,3])
    r2 = r1
    r3 = deepcopy(r1)

    print(r1, r2, r3)
    r2.c.append(4)
    print(r1, r2, r3)

    print(r1 is r2, r1 is r3)


run_3()
