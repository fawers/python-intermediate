from typing import List, Tuple
from contextlib import contextmanager


class NameAgeReaderWriter:
    def __init__(self, filename: str):
        self.records: List[Tuple[str, int]] = []  #[("name", 0), ("name", 1)]
        self.filename = filename
        self.file = None

    @contextmanager
    def read(self):
        if self.file is not None:
            raise Exception("File already opened")

        self.file = open(self.filename, 'r')

        self.records.clear()
        for record in self.file:
            name, age = record.strip().split(',')
            age = int(age)
            self.records.append((name, age))

        self.file.close()

        yield self.records[:]

        self.file = None

    @contextmanager
    def write(self):
        if self.file is not None:
            raise Exception("File already opened")

        self.file = open(self.filename, 'w')

        yield self.records

        for (name, age) in self.records:
            self.file.write(f"{name},{age}\n")

        self.file.close()
        self.file = None


def run_1():
    narwhal = NameAgeReaderWriter("name_age.csv")

    with narwhal.read() as copy_of_records:
        print(copy_of_records)

        with narwhal.read() as snd_read:
            print("exception?")


def run_2():
    narwhal = NameAgeReaderWriter("name_age.csv")

    with narwhal.read() as copy_of_records:
        print(copy_of_records)

    with narwhal.write() as records:
        records.append(('Person E', 21))

    with narwhal.read() as records_copy:
        print(records_copy)


run_2()