from typing import List

# <body id="main">

class Tag:
    def __init__(self, name, id):
        self.name = name
        self.id = id

    def __str__(self):  # string representation of object
        return f'<{self.name} id="{self.id}">'

    def __repr__(self):  # generic representation of object
        return f"{self.__class__.__name__}({repr(self.name)})"

    def to_string(self):
        return f'<{self.name} id="{self.id}">'

    @classmethod
    def create_from_string(cls, tag_string: str):
        if not cls.validate_string(tag_string):
            raise TypeError('not a tag string')

        # <body id="main">
        tag_string = tag_string[1:-1]
        # body id="main"
        name, id = tag_string.split()
        # name = 'body', id = 'id="main"'
        id_opening_quote = id.find('"')
        id_closing_quote = id.rfind('"')
        id = id[id_opening_quote+1 : id_closing_quote]
        # 'main'
        return cls(name, id)


    @staticmethod
    def validate_string(tag_as_string: str):
        # <body id="main">
        return (
            tag_as_string.startswith("<") and
            tag_as_string.endswith(">") and
            tag_as_string.find("id=") != -1
        )


def validate_tag_string(tag_as_string: str):
    return (
            tag_as_string.startswith("<") and
            tag_as_string.endswith(">") and
            tag_as_string.find("id=") != -1
    )

# a = str(obj)
# b = obj.__str__()

# c = repr(obj)
# d = obj.__repr__()

# print(obj) -> print(str(obj))
# print([obj]) -> print(str([obj])) -> print("[" + repr(obj) + "]")

class Element(Tag):
    pass


def run_1():
    body = Tag('body', 'main')
    tags: List[Tag] = [body, Element('div', 'content')]
    print(body, tags[1])    # <- will use __str__
    print(tags)  # <- will use __repr__
    print(repr(body))
    print([str(tag) for tag in tags])
    print(body.to_string())


def run_2():
    tag = Tag('body', 'main')
    tag = Tag.create_from_string('<body id="main">')
    print(repr(tag))


run_2()