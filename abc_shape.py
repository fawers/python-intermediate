#import abc
from abc import ABC, abstractmethod
import math


class Shape(ABC):
    @abstractmethod
    def perimeter(self):
        pass

    @abstractmethod
    def area(self):
        pass

    def print_info(self):
        print("We are in the Shape class")


class StraightLines:
    def print_info(self):
        print("We are in the StraightLines class")


class Rectangle(StraightLines, Shape):
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def perimeter(self):
        return 2 * self.width + 2 * self.height

    def area(self):
        return self.width * self.height


class Circle(Shape):
    def __init__(self, radius):
        self.radius = radius

    def perimeter(self):
        return 2 * math.pi * self.radius

    def area(self):
        return math.pi * self.radius ** 2


class Triangle(Shape):
    pass


def run_1():
    r = Rectangle(2, 3)
    c = Circle(3)

    print("Rectangle perimeter: ", r.perimeter())
    print("Rectangle area:      ", r.area())

    text_pieces = ["Circle perimeter:", "Circle area:"]
    phrase_length = len(max(text_pieces))
    # TODO explain in more detail
    print("%*s " % (phrase_length, text_pieces[0]), c.perimeter())
    print("%*s " % (phrase_length, text_pieces[1]), c.area())


def run_2():  # Trying to instantiate abstract class Shape
    s = Shape()  # TypeError
    print("Shape area: ", s.area())


def run_3():  # Method Resolution Order - MRO
    print(Rectangle.__mro__)
    r = Rectangle(5, 5)
    r.print_info()  # Chooses StraightLines.print_info
    Shape.print_info(r)
