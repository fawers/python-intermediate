class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other: 'Point'):
        #return self.x == other.x and self.y == other.y
        return (self.x, self.y) == (other.x, other.y)

    def __ne__(self, other):
        return not self == other

    def __lt__(self, other: 'Point'):  # lt == less than
        return (self.x, self.y) < (other.x, other.y)

    def __le__(self, other):
        return self == other or self < other

    def __gt__(self, other):
        return not self <= other

    def __ge__(self, other):
        return not self < other

    def __call__(self, offset: int):
        return Point(self.x + offset, self.y + offset)

    def __repr__(self):
        return f"Point({self.x}, {self.y})"

def run_1():
    a, b = Point(2, 2), Point(3, 3)
    print(a == b)
    print(a < b)
    print(a <= b)
    print(b >= a)
    print(a(8))


run_1()