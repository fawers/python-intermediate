v = "start here"

def return_with_args(func):
    def wrapper(*args, **kwargs):
        rv = func(*args, **kwargs)
        return (args, rv)

    return wrapper


def print_information(func):
    print("Before defining the wrapper")
    print(func)

    def wrapper(*args, **kwargs):
        print(f"Running function: {func} with arguments {args}, {kwargs}")
        return func(*args, **kwargs)

    print(f"After defining the wrapper: {wrapper}")
    return wrapper


#@return_with_args  # <-- needs attention since factorial is recursive
@print_information
def factorial(n, something_else=None):
    if n <= 1:
        return 1

    else:
        return n * factorial(n - 1)


#@return_with_args
#@print_information
def split_name(name):
    return name.split()


# split_name = return_with_args(split_name)


print(factorial(4))
#print(split_name('Fabricio Werneck'))