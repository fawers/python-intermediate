import requests


ENDPOINT = "https://swapi.dev/api/people/"

response = requests.get(ENDPOINT)
people = response.json()

# from people:
# print name, birth_year, homeworld.name

while True:
    for person in people['results']:
        world_response = requests.get(person['homeworld'])
        planet = world_response.json()

        print(person['name'], person['birth_year'], planet['name'])

    if people['next'] is None:
        break

    people = requests.get(people['next']).json()


v = 0