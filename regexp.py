import re


text = "The bat saw the cat chasing the rat."
sentence = 'The quick brown fox jumped over the lazy dog'

pattern = re.compile(r"[bcr]at")

match = pattern.search(text)

# find first match
# print(match.group())

# find all matches as strings
# for m in pattern.findall(text):
#     print(m)

# find all matches as match objects
# for m in pattern.finditer(text):
#     ((start, end),) = m.regs
#     print(f"text[{start}:{end}] == {text[start:end]}")

def correctly_replace(match):
    if match.group() == 'quick':
        return 'slow'

    else:
        return 'active'


# print(re.sub(r"quick|lazy", correctly_replace, sentence))

estonian_number = '+37251234567'
estonian_number_pattern = re.compile(r"(\+372)(5\d{6,7})")

match = estonian_number_pattern.match(estonian_number)

#print(match.groups())

time_string = '12:33'
time_pattern = r"(?P<hour>\d\d):(?P<minute>\d\d)"

match = re.search(time_pattern, time_string)

print(match['minute'])
