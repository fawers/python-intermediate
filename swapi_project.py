import csv
from typing import Callable, Iterator, Optional

import requests


ENDPOINT = "https://swapi.dev/api/planets/"
CSV_FILENAME = 'planets.csv'


class Planet:
    def __init__(self, name: str, rotation: Optional[int], orbit: Optional[int],
                 climate: str, terrain: str, population: Optional[int]):
        self.name = name
        self.rotation_period = rotation
        self.orbital_period = orbit
        self.climate = climate
        self.terrain = terrain
        self.population = population


def get_planets() -> Iterator[Planet]:
    response = requests.get(ENDPOINT).json()

    while True:
        for planet in response['results']:
            for key in ('rotation_period', 'orbital_period', 'population'):
                planet[key] = (int(planet[key])
                               if planet[key].isnumeric()
                               else None)

            yield Planet(
                planet['name'], planet['rotation_period'], planet['orbital_period'],
                planet['climate'], planet['terrain'], planet['population'])

        if response['next'] is None:
            break

        response = requests.get(response['next']).json()


def filter_planets_by(predicate: Callable[[Planet], bool]) -> Iterator[Planet]:
    yield from filter(predicate, get_planets())
    # or
    #yield from (p for p in get_planets() if predicate(p))


def get_earth_like_planets() -> Iterator[Planet]:
    yield from filter_planets_by(
        lambda p: p.rotation_period is not None and 20 <= p.rotation_period <= 28)


def save_planets_to_csv():
    headers = ['Name', 'Rotation period', 'Orbital period', 'Climate', 'Terrain', 'Population']
    planets = get_earth_like_planets()

    with open(CSV_FILENAME, 'w') as csvfile:
        writer = csv.writer(csvfile, lineterminator='\n')
        print(repr(writer.dialect.lineterminator))

        writer.writerow(headers)

        for p in planets:
            data = (p.name, p.rotation_period, p.orbital_period, p.climate,
                    p.terrain, p.population)
            writer.writerow(data)


save_planets_to_csv()