import sys
import pickle


FILENAME = "generator.pickle"


def count_to_ten():
    yield from range(1, 11)


class TenCounter:
    def __init__(self):
        self.n = 1

    def __iter__(self):
        return self

    def __next__(self):
        if self.n == 11:
            raise StopIteration

        cur = self.n
        self.n += 1

        return cur


if sys.argv[1] == 'dump':
    g = TenCounter()

    for _ in range(5):
        print(next(g))

    # w - write mode; b - binary mode
    with open(FILENAME, 'wb') as f:
        pickle.dump(g, f)

elif sys.argv[1] == 'load':
    # r - read mode; b - binary mode
    with open(FILENAME, 'rb') as f:
        g = pickle.load(f)

    for v in g:
        print(v)

elif sys.argv[1] == 'ro':
    with open(FILENAME, 'rb') as f:
        print(f.read())
        print(f.writable())
        f.write(b"Trying to write to read-only file")

elif sys.argv[1] == 'rw':
    with open(FILENAME, 'a+b') as f:
        print(f.readable())
        print(f.read())
        f.write(b"New bytes")
        f.seek(0)
        f.write(b"Old bytes")