import csv
from pprint import pprint


def exclude_leave_eq_0(row: list) -> bool:
    return int(row[-1]) != 0


def read_sample_csv():
    with open('Sample100.csv') as f:
        reader = csv.reader(f)
        headers = next(reader)

        #reader = filter(exclude_leave_eq_0, reader)

        output = []

        for row in reader:
            output.append(dict(zip(headers, row)))

    return output


