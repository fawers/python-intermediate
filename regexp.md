# Regular Expressions

**What**
* A string defining a search pattern.
* Find (and optionally Replace)
* Validation

**Where**
* Text processing tools (*sed*, *AWK*)
* Text Editors and Word Processors (VI, PyCharm, MS Word)

**Why**
* Powerful patterns
* Can be quite simple
* Portable*

**Disadvantages**
* May become overwhelmingly hard to read
* Errors in complex expressions

## Resources
* https://regex101.com/
* https://debuggex.com/

## Examples
Matching the word `Apple`:

    Apple

Matching the word `Apple` in the beginning of the string:

    ^Apple

Matching the word `Apple` in the end of the string:

    Apple$

Matching a digit:

    \d

... 0 or 1 time:

    \d?

... 0 or more times:

    \d*

... 1 or more times:

    \d+

Matching either `Apple` or `Orange`:

    Apple|Orange

Matching `pineapple` or `apple`:

    (pine)?apple

Matching any of `Strawberry`, `Blueberry`, or `Blackberry`:

    (Straw|Bl(ack|ue))berry

Matching `Color` or `Colour`:

    Colou?r

Matching `Gray` or `Grey`:

    Gr[ae]y

Matching a six-digit hexadecimal color code (e.g., `#F1620B`), uppercase:

    #[A-F0-9]{6}

Matching a date string (e.g., `2020-09-27`), 2000 up to 2099:

    20\d\d-(0\d|1[012])-([0-2]\d|3[01])

**Exercise:** write a regular expression that can match any of `cat`, `bat`, `rat`.

**Exercise:** write a regular expression that can match either `SDA` or `Software Development Academy`.

***

Example of "write-only" regular expression:

    02-[012]\d|(?=1[02]|0[13578])\d\d-(?:[012]\d|3[01])|(?=0[469]|11)\d\d-(?:[012]\d|30)

>!["debuggex"](regexp.png)
>https://debuggex.com/

The one above matches a month and a day in the format `MM-DD`, validating possible days for each
month (up to 31 for months 1, 3, 5, 7, 8, 10, and 12; up to 30 for months 4, 6, 9, and 11; up to 29
for month 2).

***

## Concepts

### Boolean "or"
* `A|B` matches `A` or `B`

### Grouping
* `(straw|blue)berry` matches `strawberry` or `blueberry`, and stores either `straw` or `blue` in group 1.
* `(?:straw|blue)berry` matches `strawberry` or `blueberry`, but doesn't store anything in any group.
* `(?P<kind>straw|blue)berry` matches `strawberry` or `blueberry`, and stores `straw` or `blue` in group `kind`.

### Quantifiers
* `6?` matches `6` zero or one time (i.e., optional match).
* `6*` matches `6` zero or more times.
* `6+` matches `6` one or more times.
* `6{3}` matches `6` exactly `3` times.
* `0{2,4}` matches `0` `2`, 3, or `4` times.
* `4{2,}` matches `4` *at least* `2` times.
* `(?:138){,5}` matches `138` *at most* `5` times.

**Exercise:** Write a regular expression that can match any binary number of length 8, 16, or 32.

### Metacharacters

| character | brief description | example | matches |
|:-:|:-:|:-:|:-:|
| `^` | matches the beginning of the string | `^Hello` | `"Hello"` but not `" Hello"` |
| `$` | matches the end of the string | `world$` | `"world"` but not `"world."` |
| `.` | matches any character | `H.llo` | `"Hello"`, `"Hallo"` |
| `[xyz]` | matches one of `x`, `y`, or `z` once | `[ab][cd]` | `ac`, `ad`, `bc`, `bd` |
| `[0-6]` | matches one of 0, 1, 2, 3, 4, 5, 6 once | `[a-c]` | `a`, `b`, `c` |
| `[^xyz]` | matches any character except `x`, `y`, or `z` | `[^a]` | anything but `a` |
| `\3` | matches the contents of group `3` | `(mi)\1c` | `mimic` |

### Character classes

| character class | equivalent range pattern | description |
|:-:|:-:|:-:|
| `\w` | `[_A-Za-z0-9]` | alphanumeric |
| `\W` | `[^_A-Za-z0-9]` | non-alphanumeric |
| `\d` | `[0-9]` | digit |
| `\D` | `[^0-9]` | non-digit |
| `\s` | `[ \t\r\n\v\f]` | whitespace |
| `\S` | `[^ \t\r\n\v\f]` | non-whitespace |
| `\b` | N/A | word boundaries |

***

## Assertions
### Look-ahead
Asserts that the string is or is not followed by some pattern, but doesn't match it.

**Asserts that a lowercase letter is followed by an uppercase letter:**

    [A-Z](?=[a-z])

**Asserts that an uppercase letter is not followed by a lowercase letter:**

    [a-z](?![A-Z])

### Look-behind
Asserts that the string is or is not to the right of some pattern, but doesn't match it.

**Asserts that `5` comes immediately after `372`:**

    (?<=372)5
    
**Asserts that `email` does not follow `@`:**

    (?<!@)email