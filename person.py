class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age


class Student(Person):
    def __init__(self, name: str, age: int, scholarship: float):
        print("In Student __init__")
        if self.is_name_correct(name):
            Person.__init__(self, name, age)
            self.scholarship = scholarship

    def show_finance(self):
        return self.scholarship

    @classmethod
    def create_from_string(cls, sentence):
        # sentence is sth like "Fabricio 28 0"
        name, age, scholarship = sentence.split()  # ["Fabricio", "28", "0"]
        age, scholarship = int(age), float(scholarship)
        if cls.is_name_correct(name):
            return cls(name, age, scholarship)
            # Student.create_from_string("Fabricio 28 0")
            # same as:
            # return Student("Fabricio", 28, 0)
            # TransferredStudent.create_from_string("Fabricio 28 0")
            # same as:
            # TransferredStudent("Fabricio", 28, 0)

    @staticmethod
    def is_name_correct(name):
        return name[0].isupper() and len(name) > 3


def is_name_correct(name):
    return name[0].isupper() and len(name) > 3


class TransferredStudent(Student):
    def __init__(self, name, age, scholarship):
        print("In TransferredStudent __init__")
        super().__init__(name, age, scholarship)



student = Student.create_from_string("Fabricio 28 0")
ts = TransferredStudent.create_from_string("Fabricio 28 0")
print(Student.is_name_correct("Fabricio"))
print(TransferredStudent.is_name_correct("fabricio"))