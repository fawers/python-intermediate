from datetime import datetime


def disable_at_night(func):
    # a decorator that only calls a decorated function during the day
    def wrapper():
        if 7 <= datetime.now().hour < 22:
            func()

    return wrapper


def print_time_of_call(func):
    # a decorator that only calls a decorated function during the day
    def wrapper():
        print(f"Function called at {datetime.now()}")
        func()

    return wrapper


def say_hello():
    print("Hello world")


say_hello = print_time_of_call(print_time_of_call(say_hello))


#@print_time_of_call
#@print_time_of_call
@print_time_of_call
def fabs_greetings():
    print('Good day!')


#say_hello()
#fabs_greetings()

# --------------- with arguments


def run_only_between(from_=7, to_=22):
    # a decorator that only calls a decorated function at certain times
    def decorator(func):
        def wrapper():
            if from_ <= datetime.now().hour < to_:
                func()
        return wrapper
    return decorator


@run_only_between(10, 15)
def say_something():
    print("Hello world")


run_only_between_12_and_16 = run_only_between(12, 16)


@run_only_between_12_and_16
def say_something_12_16():
    print('Hello world from 12 to 16')


say_something()