from typing import Iterator, Callable, Optional, TypeVar


T = TypeVar('T')


def sequencer(initial_value: T, transformer: Callable[[T], Optional[T]]) -> Iterator[T]:
    """
    Write a generator function that can do any transformation on its
    initial value to get the next one.
    The transformer should determine if the sequence is finite or not.
    """
    yield initial_value

    v = transformer(initial_value)

    while v is not None:
        yield v
        v = transformer(v)


def add_bangs(s: str) -> Optional[str]:
    if len(s) >= 15:
        return None

    return s + '!'


# def up_to_10(n: int) -> Optional[int]:
#     if n >= 10:
#         return None
#
#     return n + 1


for (_, value) in zip(range(20), sequencer(102, lambda n: n // 2 if n > 0 else None)):
    print(value)


# Zip input:
# a: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# b: [1, 100, 10000]
#
# Zip output:
# [(0, 1), (1, 100), (2, 10000), ...]