from math import sqrt
from timeit import timeit


setup = "from math import sqrt"

code = '''
def func():
    return [sqrt(x) for x in range(100)]
'''

def test_func():
    return [sqrt(x) for x in range(100)]

print(timeit(stmt=test_func, number=1000))
