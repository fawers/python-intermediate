

def inc(n: int) -> int:
    #raise ValueError("invalid value")
    return n + 1

#print(inc(99))

increase = lambda n: n + 1

#print(increase(999))

increase2 = inc

#print(increase2(9))

# print(inc)
# print(dir(inc))
# print(inc.__annotations__)
# print(inc.__code__)
# print(dir(inc.__code__))

# print(increase)
# print(dir(increase))
# print(increase.__annotations__)

if inc(1) % 2 == 0:
    w = 100
    v = 10
else:
    v = 15

v = 10 if inc(1) % 2 == 0 else 15

my_lambda = lambda x: x.lower()
my_lambda("HA HA HA")  # "ha ha ha"

def my_function(x: str) -> str:
    return x.lower()

square_lambda = lambda x: x ** 2
square_lambda(4)  # 16
def square_function(x: int) -> int:
    return x ** 2

equals_lambda = lambda x, y: x == y
equals_lambda(1, 2)  # False
def equals_function(x, y) -> bool:
    return x == y

#print((lambda n: n + 2)(4))

items = [1, 2, 3, 4, 5]

even = lambda n: n % 2 == 0
odd = lambda n: not even(n)

squared = list(map(lambda x: x ** 2, items))  # [1, 4, 9, 16, 25]
square_comp = [x ** 2 for x in items]

odds = list(filter(odd, items))  # [1, 3, 5]
odds_comp = [x for x in items if x % 2 != 0]

filter_and_map = [x ** 2 for x in items if even(x)]

# ^^^ same as:
# odds_notmap = []
# for i in items:
#     if i % 2 == 1:
#         odds_notmap.append(i)

# print('3 % 2', 3 % 2, bool(3 % 2))
# print('8 % 2', 8 % 2, bool(8 % 2))
# print('9 % 5 =', 9 % 5)
# n = 64
# print(f'127 % {n} =', 127 % n)

items = [1, 2, 3, 4, 5]

from functools import reduce
items_sum = reduce(lambda x, y: x + y, items)  # 15
# v = 1 + 2 = 3
# v = v + 3 = 3 + 3 = 6
# v = v + 4 = 10
# v = v + 5 = 15
#print(items_sum)

nested_list = [[1,2,3],[4,5,6],[7,8,9]]
flattened_list = reduce(lambda x, y: x + y, nested_list)

# print(nested_list)
# print(flattened_list)

def run_1():
    nested_list = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10,11,12],[13,14,15]]
    operation = lambda x, y: x + y
    first, second, *tail = nested_list
    output = operation(first, second)

    for l in tail:
        output = operation(output, l)

    print(output)


def run_2():
    name_parts = ['Fabricio', 'da Silva', 'Werneck']

    def operation(x: str, y: str) -> str:
        return f"{x} {y}"

    first, second, *tail = name_parts

    output = operation(first, second)
    for part in tail:
        output = operation(output, part)

    print(output)


def run_3():
    name_parts = ['Fabricio', 'da Silva', 'Werneck']
    print(reduce(lambda x, y: f"{x} {y}", name_parts))


def run_4():
    pairs = [(1, 10), (2, 9), (2, 7), (3, 8), (3, 6), (1, 15)]

    #print(sorted(pairs, key=lambda x: x[1]))  # [(3, 8), (2, 9), (1, 10)]
    print(sorted(pairs, key=lambda x: x[0]))  # [(1, 10), (2, 9), (3, 8)]
    print(sorted(pairs, key=lambda x: (x[0], x[1] )  ) )
    print(sorted(pairs, key=lambda x: (x[0], -x[1])  ) )
    #print(sorted(pairs, key=lambda x: x[1], reverse=True))  # [(1, 10), (2, 9), (3, 8)]
    #print(sorted(pairs, key=lambda x: -x[0]))  # [(3, 8), (2, 9), (1, 10)]

    reversed_tuples = [(y, x) for x, y in pairs]
    #print(sorted(reversed_tuples, reverse=False))

    min(pairs)  # (1, 10)
    max(pairs, key=lambda x: x[1])  # (1, 10)
    max(pairs, key=lambda x: x[0] * x[1])  # (3, 8)

    pairs = [(1, 10), (1, 15)]
    print(max(pairs, key=lambda x: (x[0], -x[1])))


run_4()