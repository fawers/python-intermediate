from abc import ABC, abstractmethod


class Database(ABC):
    def __init__(self, host):
        self.host = host

    @abstractmethod
    def connect(self):
        pass


class RemoteDatabase(Database):
    def __init__(self, host, user, password):
        super().__init__(host)
        self.user = user
        self.password = password

    def connect(self):
        try:
            conn = Connection(self.host)
            conn.auth(self.user, self.password)
            return conn

        except NoInternetError:
            pass

        except HostDownError:
            pass

        except InvalidCredentialsError:
            pass


class LocalDatabase(Database):
    def __init__(self):
        super().__init__('localhost')

    def connect(self):
        try:
            return Connection(self.host)

        except DatabaseDoesntExistLocallyError:
            pass


rd1 = RemoteDatabase('database.google.com', 'admin', 'admin')
rd2 = RemoteDatabase('172.10.1.17')
rd3 = RemoteDatabase('127.0.0.1')  # localhost ip address
rd4 = RemoteDatabase('localhost')
ld = LocalDatabase()

rd1.connect()  # <- self is rd1
RemoteDatabase.connect(rd2)  # <- self is rd2
