class Repeater:
    def __init__(self, value):
        self.value = value

    def __iter__(self):
        return self

    def __next__(self):
        return self.value


def run_1():
    repeater = Repeater('Python')

    for v in repeater:
        print(v)


class Countdown:
    def __init__(self, number: int):
        if number <= 0:
            raise ValueError(f"'number' must be > 0 (is {number})")

        self.number = number

    def __iter__(self):
        return self

    def __next__(self):
        current_value = self.number
        self.number -= 1

        if current_value == -1:
            raise StopIteration

        return current_value


def run_2():
    cd = Countdown(5)

    for v in cd:
        print(v)


def repeat_n_k_times(n, k):
    for _ in range(k):
        yield n


def run_3():
    repeater = repeat_n_k_times(10, 5)

    for value in repeater:
        print(value)


# i1 = Repeater(10)
# i2 = Countdown(10)
# g = repeat_n_k_times(10, 5)
#
# print(i1, i2, g)


def run_4():
    squares = [n * n for n in range(1, 6)]
    # generator expression: syntactically the same as
    # list-comprehensions, but with
    # parentheses instead of square brackets
    squares_gen = (n * n for n in range(1, 6))

    for v in squares_gen:
        print(v)
